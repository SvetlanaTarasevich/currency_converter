({
    init: function (component, event, helper) {
        helper.initHelp(component, event, helper);
    },

    sendValuesDates: function (component, event, helper) {
        helper.getRatesByDates(component, event, helper);
    },

    getPicklistValue: function (component, event, helper) {
        helper.getPicklistValueHelp(component, event, helper);
    },

    getBaseCurrency: function (component, event, helper) {
        helper.getBaseCurrencyHelp(component, event, helper);
    }
})