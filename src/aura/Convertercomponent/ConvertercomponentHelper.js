({
    initHelp: function (component, event, helper) {
        var baseCurrency = 'GBP';
        var action = component.get("c.getAllCurrencyFromBase");
        component.set('v.columns', [
            {label: 'USD', fieldName: 'USD__c', type: 'currency'},
            {label: 'EUR', fieldName: 'EUR__c', type: 'currency'},
            {label: 'CAD', fieldName: 'CAD__c', type: 'currency'},
            {label: 'GPB', fieldName: 'GBP__c', type: 'currency'},
            {label: 'Base Currency', fieldName: 'Base_Currency__c', type: 'text'},
            {label: 'Date', fieldName: 'Date__c', type: 'text'}
        ]);
        action.setParams({
            baseCurrency: baseCurrency
        });
        // console.log(baseCurrency);

        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.table", result);
                // console.log('allrates' + result);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },

    getRatesByDates: function (component, event, helper) {
        var startDate = component.find("valueDateStart").get("v.value");
        var endDate = component.find("valueDateEnd").get("v.value");
        var action = component.get("c.getRatesFromDate");
        action.setParams({
            startDate: startDate,
            endDate: endDate
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.table", result);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },

    getBaseCurrencyHelp: function (component, event, helper) {
        var baseCurrency = component.find("currency").get("v.value");
        console.log(baseCurrency);
        var action = component.get("c.getAllCurrencyFromBase");
        action.setParams({
            baseCurrency: baseCurrency
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.table", result);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },

    getPicklistValueHelp: function (component, event, helper) {
        var action = component.get("c.getPicklist");
        action.setCallback(this, function (response) {
            var result = response.getReturnValue();
            component.set("v.varPicklist", result);
        });
        $A.enqueueAction(action);
    },

})