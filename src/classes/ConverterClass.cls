public with sharing class ConverterClass {
    @AuraEnabled
    public static List<Exchange_Rate__c> getAllCurrencyFromBase(String baseCurrency) {

        List<Exchange_Rate__c> listAllCurrency = [
                SELECT CAD__c, USD__c, EUR__c, GBP__c, Dates__c, Base_Currency__c
                FROM Exchange_Rate__c
                WHERE Base_Currency__c = :baseCurrency
                ORDER BY Dates__c
                        DESC
        ];
        return listAllCurrency;
    }


    @AuraEnabled
    public static void getRatesFromDate(Date startDate, Date endDate) {
        List<String> listStringsDate = new List<String>();
        List<Exchange_Rate__c> listExchangeRates = new List<Exchange_Rate__c>();
        while (startDate <= endDate) {
            listStringsDate.add(String.valueOf(startDate));
            startDate = startDate.addDays(1);
        }

        List<String> listSelectOption = ConverterClass.getPicklist();

        for (String itemDate : listStringsDate) {
            for (String itemOptions : listSelectOption) {
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                String url = 'https://api.exchangeratesapi.io/' + itemDate + '?base=' + itemOptions;
                request.setMethod('GET');
                request.setEndpoint(url);
                HttpResponse response = http.send(request);
                String stringResponce = (response.getBody()).replace('date', 'dateValue');
                fromJSON wrapperClass = fromJSON.parse(stringResponce);
                Exchange_Rate__c exchangeRate = new Exchange_Rate__c(
                        USD__c = wrapperClass.rates.USD,
                        EUR__c = wrapperClass.rates.EUR,
                        CAD__c = wrapperClass.rates.CAD,
                        GBP__c = wrapperClass.rates.GBP,
                        Dates__c = Date.valueOf(itemDate),
                        Base_Currency__c = wrapperClass.base
                );
                listExchangeRates.add(exchangeRate);
            }
        }
        upsert listExchangeRates;
    }

    @AuraEnabled
    public static List<String> getPicklist() {
        List<String> options = new List<String>();

        Schema.SObjectType objType = Exchange_Rate__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        List<Schema.PicklistEntry> values = fieldMap.get('Base_Currency__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values) {
            options.add(a.getLabel());
        }
        return options;
    }
}