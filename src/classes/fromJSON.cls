public class fromJSON {
    public String base;	//USD
    public String dateValue;	//2019-11-01
    public rates rates;
    public class rates {
        public Double CAD;	//1.3180716402
        public Double HKD;	//7.8371487566
        public Double ISK;	//123.9788131789
        public Double PHP;	//50.5305682736
        public Double DKK;	//6.7072448155
        public Double HUF;	//294.7571595296
        public Double CZK;	//22.9051081785
        public Double GBP;	//0.7721339438
        public Double RON;	//4.2685160248
        public Double SEK;	//9.6052607954
        public Double IDR;	//14041.5926025676
        public Double INR;	//70.7568004309
        public Double BRL;	//3.9893168148
        public Double RUB;	//63.8105754556
        public Double HRK;	//6.697190053
        public Double JPY;	//108.1156297693
        public Double THB;	//30.1849358111
        public Double CHF;	//0.9886883921
        public Double EUR;	//0.8977466559
        public Double MYR;	//4.1655444833
        public Double BGN;	//1.7558129096
        public Double CNY;	//7.0419247688
        public Double NOK;	//9.1245174612
        public Double NZD;	//1.555435856
        public Double ZAR;	//15.1072807254
        public Double USD;	//1
        public Double MXN;	//19.1367268157
        public Double SGD;	//1.3582009157
        public Double AUD;	//1.4499506239
        public Double ILS;	//3.525630667
        public Double KRW;	//1167.1514498608
        public Double PLN;	//3.8185654008
    }
    public static fromJSON parse(String json){
        return (fromJSON) System.JSON.deserialize(json, fromJSON.class);
    }
}